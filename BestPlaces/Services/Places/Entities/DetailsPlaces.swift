//
//  DetailsPlaces.swift
//  BestPlaces
//
//  Created by Ronilson Batista on 21/04/19.
//  Copyright © 2019 Ronilson Batista. All rights reserved.
//

import Foundation

struct DetailsPlaces: Codable {
    
    let result: DetailsResult
    
    enum CodingKeys: String, CodingKey {
        case result
    }
}
