//
//  ServiceEncoding.swift
//  BestPlaces
//
//  Created by Ronilson Batista on 19/04/19.
//  Copyright © 2019 Ronilson Batista. All rights reserved.
//

enum ServiceEncoding {
    case `default`
    case json
}
