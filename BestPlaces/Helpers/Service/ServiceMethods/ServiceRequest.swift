//
//  ServiceRequest.swift
//  BestPlaces
//
//  Created by Ronilson Batista on 19/04/19.
//  Copyright © 2019 Ronilson Batista. All rights reserved.
//

import Foundation

final class ServiceRequest {
    static let shared: ServiceManagerProtocol = ServiceManager()
}
